<?php

class Index_controller extends Controller {

	function __construct() {
		parent::__construct();
	}
        
        public function getIndex(){
            echo "Get method Index controller";
        }
        
        public function postIndex(){
            echo "Post method Index controller";
        }
        
        public function getCliente($a,$b,$c = array()){
            $cURLC = new CUrlClient($a,1,true);
            $r = $cURLC->execute($b, $c);
            Penelope::printJSON($r);
        }
        
        public function getSaludo($nombre,$apellido){
            if (!isset($nombre) || !isset($apellido)) {  
                throw new Exception('Paremetros insuficientes.');
            }
            Request::setHeader(200,"text/plain");
            echo "Hey a".$nombre." ".$apellido."!";
        }
}
