<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Banco_controller
 *
 * @author AndresG
 */
class Bancos_controller extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function getBancos($id = null){
        if(isset($id) && !is_null($id)){
            Penelope::printJSON(Bancos::getById($id));
        }else{
            Penelope::printJSON(Bancos::getAll());
        }
    }
    
    public function postBancos(){
        
    }
}

spl_autoload_register(function($class) {
    if (file_exists("models/co.edu.usbcali.cuentas/" . $class . ".php")) {
        require "models/co.edu.usbcali.cuentas/" . $class . ".php";
    }
});