<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cuentas_controller
 *
 * @author AndresG - sebas7103
 */
class Cuentas_controller extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function getCuentas($id = null){
            if(isset($id) && !is_null($id)){
                $sth = Cuentas::getById($id);
                ms(Hash::encrypt("sebasflow", HASH_PASSWORD_KEY));
                Penelope::printJSON($sth->toArray());
            }else{
                Penelope::printJSON(Cuentas::getAll());
            }
        }
        
        public function postCuentas(){
            $keys = Cuentas::getKeys();
            $a = true;
            $this->validateKeys($keys, filter_input_array(INPUT_POST));
            $cnt = Cuentas::instanciate(filter_input_array(INPUT_POST));
            $cnts = Cuentas::where("cedula",$cnt->getCedula());
            foreach ($cnts as $key => $value) {
                if($value["Bancos_id"] == $cnt->getBancos_id() && $value["Tipo_cuentas_id"] == $cnt->getTipo_cuentas_id()){
                    $type = Tipo_cuentas::getById($value["Tipo_cuentas_id"]);
                    $banco = Bancos::getById($value["Bancos_id"]);
                    $a = false;
                    break;
                }
            }
            if($a){
                $r = $cnt->create();
                if($r["error"] == 0){
                     $msg = $r["msg"];
                     $data = $cnt->toArray();
                     $data["password"] = Hash::encrypt($data["password"], HASH_PASSWORD_KEY);
                     $args = ["user"=>  Penelope::arrayToJSON($data)];
                 }else{
                     $msg = $r["msg"];
                     $args = [];
                 }
            }else{
                $r = array("error"=>1);
                $msg = "Ya existe esta cuenta ".$type->getNombre()."  en el banco ".$banco->getNombre();
                $args = [];
            }
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
        }
        
        public function putCuentas(){
            $_PUT = $this->_PUT;
            
            $cnts = Cuentas::getById($_PUT["id"]);
            foreach ($_PUT as $key => $value) {
                if($key == "password"){
                    $password = $_PUT["password"];
                    $cnts->{"set".ucfirst($key)}($password);
                }else{
                    $cnts->{"set".ucfirst($key)}($value);
                }
            }
            $r = $cnts->update();
            
            if($r["error"] == 0){
                $msg = $r["msg"];
                $data = $cnts->toArray();
                $data["password"] = Hash::encrypt($data["password"], HASH_PASSWORD_KEY);
                $args = ["user"=>  Penelope::arrayToJSON($data)];
            }else{
                $msg = $r["msg"];
                $args = [];
            }
            
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
        }
        
        public function deleteCuentas(){
            $_DELETE = $this->_DELETE;
            $usr = Cuentas::getById($_DELETE["id"]);
            if($usr->getId() != NULL){
                $r = $usr->delete();
                if($r["error"] == 0){
                    $msg = $r["msg"];
                    $args = ["user"=>  Penelope::arrayToJSON($usr->toArray())];
                }else{
                    $msg = $r["msg"];
                    $args = [];
                }
            }else{
                $r["error"]=1;
                $msg = "Invalid User to Delete";
                $args = [];
            }
            
            $response = Request::response($msg, $args, $r["error"]);
            Penelope::printJSON($response);
            
        }
        
        public function getAttr($id, $attr){
            if($attr == "Bancos_id" || $attr == "Tipo_cuentas_id"){
                $cnt = Cuentas::getById($id);
                if($cnt){
                    $sth = Cuentas::whereR($attr,"id",$id,"cuentas");
                    $rError = 0;
                    $class = explode("_id",$attr);
                    $obj = $class[0]::getById($sth[0][$attr]);
                    $msg = "Cuenta existente";
                    $nombre = [$obj->getNombre()];
                    $args = [$class[0] =>  Penelope::arrayToJSON($nombre)];
                }else{
                    $rError = 1;
                    $msg = "Cuenta Inexistente";
                    $args = [];
                }
            }else{
                $rError = 1;
                $msg = "No tiene permiso para acceder a este dato";
                $args = []; 
            }
                $response = Request::response($msg, $args, $rError);
                Penelope::printJSON($response);
        }
        
        public function getResume($password, $id = null){
            $cnt = Cuentas::getById($id);
            if($cnt){           
                if(Hash::decrypt($password, HASH_PASSWORD_KEY) == $cnt->getPassword()){
                   $rError = 0;
                   $banco = Bancos::getById($cnt->getBancos_id());
                   $type = Tipo_cuentas::getById($cnt->getTipo_cuentas_id());
                   $msg = "Cuenta Verificada";
                   $args = ["cuenta" =>  Penelope::arrayToJSON(["Cedula"=>$cnt->getCedula(),
                                                                 "Banco"=>$banco->getNombre(),
                                                                 "Tipo de cuenta"=>$type->getNombre(),
                                                                 "Saldo"=>$cnt->getSaldo()])];
                }else{
                    $rError = 1;
                    $msg = "contraseña invalida";
                    $args = [];
                }
            }else if(Hash::decrypt($password) ==  HASH_SECRET ){
                $rError = 0;
                $cnts = Cuentas::getAll();
                $data = array();
                foreach ($cnts as $key => $value) {
                    $transacciones = Model::whereR("Transacciones_id",array("Cuentas_id","Tipo_transacciones_id"), array($value["id"],2), "Transacciones_registro");
                    $data[] = array("cedula"=>$value["cedula"],
                                    "transacciones"=>array());
                    $dataTransacciones = array();
                    foreach ($transacciones as $key2 => $value2) {
                        $transaccion = Transacciones::getById($value2["Transacciones_id"]);
                        $vars = $transaccion->getMyVars();
                        $dataTransacciones[] = array("fecha"=>$vars["fecha"],
                                                     "monto"=>$vars["monto"]);    
                    }
                    $data[count($data)-1]["transacciones"] = Penelope::arrayToJSON($dataTransacciones);
                }
                $args = ["cuentas" =>  Penelope::arrayToJSON($data)];
            }else{
                $rError = 1;
                $msg = "Cuenta inexistente";
                $args = [];
            }     
        $response = Request::response($msg, $args, $rError);
        Penelope::printJSON($response);
        }
        
        public function getSaldo($cedula, $password){
            $cnts = Cuentas::where("cedula",$cedula);
            if($cnts){           
                if(Hash::decrypt($password) ==  HASH_SECRET ){
                   $rError = 0;
                   $msg = "Cuenta Verificada";
                   $data = array();
                   foreach ($cnts as $key => $value) {
                       $data[] = array("saldo"=>$value["saldo"]
                                        );
                   }
                   $args = ["Saldos" =>  Penelope::arrayToJSON($data)];
                }else{
                    $rError = 1;
                    $msg = "contraseña invalida";
                    $args = [];
                }
            }else{
                $rError = 1;
                $msg = "Cuenta inexistente";
                $args = [];
            }
        $response = Request::response($msg, $args, $rError);
        Penelope::printJSON($response);
            
        }
}

spl_autoload_register(function($class) {
    if (file_exists("models/co.edu.usbcali.cuentas/" . $class . ".php")) {
        require "models/co.edu.usbcali.cuentas/" . $class . ".php";
    }
});
