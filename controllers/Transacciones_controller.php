<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transacciones
 *
 * @author AndresG
 */
class Transacciones_controller extends Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function getTransacciones($id = null){
            if(isset($id) && !is_null($id)){
                $sth = Transacciones::getById($id);
                Penelope::printJSON($sth->toArray());
            }else{
                Penelope::printJSON(Transacciones::getAll());
            }
        }
        
    public function postTransacciones(){
        $cnts = Cuentas::getById(filter_input(INPUT_POST, "Cuentas_id"));
        if($cnts){
            $tipo_transaccion = Tipo_transacciones::getById(filter_input(INPUT_POST,"Tipo_transacciones_id"));
            $type = (isset($tipo_transaccion))? $tipo_transaccion->getNombre() : null;
            $trns = Transacciones::instanciate(filter_input_array(INPUT_POST));
            $trns->setFecha(date("Y-m-d H:i:s"));
            $monto = filter_input(INPUT_POST,'monto');
            $remitente = Cuentas::getById(filter_input(INPUT_POST, "id_remitente"));
            if($type == "generar"){
                if($remitente){
                    $rError = 0;
                    $cntAct_remitente = $remitente->toArray();
                    $costo_transaccion = ($cnts->getBancos_id() == $remitente->getBancos_id())? 4000 : 7000;
                    $cntAct_remitente["saldo"] -= $monto+$costo_transaccion;
                    $r = $trns->create();
                    $trns->has_many('cuentas', $cnts,['Tipo_transacciones_id'=>1,"Coste_transaccion"=>0]);
                    $trns->has_many('cuentas', $remitente,['Tipo_transacciones_id'=>$tipo_transaccion->getId(),"Coste_transaccion"=>$costo_transaccion]);
                    $trns->update();
                    $msg = "transaccion creada";
                    $args = ["transacion"=>  Penelope::arrayToJSON($trns->toArray())];   
                    $cntAct = $cnts->toArray();
                    $cntAct["saldo"] += $monto;
                    $this->getCliente(_URL."Cuentas", "PUT", $cntAct);
                    $this->getCliente(_URL."Cuentas", "PUT", $cntAct_remitente);
                }else{
                    $rError = 1;
                    $msg = "La cuenta del remitente no existe";
                    $args = [];
                }
            }else if($type == "depositar"){
                $rError = 0;
                $r = $trns->create();
                $trns->has_many('cuentas', $cnts, ['Tipo_transacciones_id'=>1,"Coste_transaccion"=>0]);
                $trns->update();
                $msg = "transaccion creada";
                $args = ["transacion"=>  Penelope::arrayToJSON($trns->toArray())];   
                $cntAct = $cnts->toArray();
                $cntAct["saldo"] += $monto;
                $this->getCliente(_URL."Cuentas", "PUT", $cntAct);
            }else{
                $rError = 1;
                $msg = "El tipo de transaccion no es valida";
                $args = [];
            }     
        }else{
                $rError = 1;
                $msg = "La cuenta no existe";
                $args = [];
        }
        $response = Request::response($msg, $args, $rError);
        Penelope::printJSON($response);
        }

}

spl_autoload_register(function($class) {
    if (file_exists("models/co.edu.usbcali.cuentas/" . $class . ".php")) {
        require "models/co.edu.usbcali.cuentas/" . $class . ".php";
    }
});
