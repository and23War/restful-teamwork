<?php

/*
 * RESTful API Example
 * 
 * Created by:
 * @Pabhoz
 * pabhoz[@]gmail.com
 * 
 */

require 'config.php';

spl_autoload_register(function($class){
    if(file_exists(_LIBS.$class.".php")){
        require _LIBS.$class.".php";
    }else{
       if(file_exists(_INTERFACES.$class.".php")){
            require _INTERFACES.$class.".php";
        } 
    }
});

//Capturamos la URL
$url = ( filter_input(INPUT_GET,"url") != null) ? filter_input(INPUT_GET,"url") : "Index/";
$url = explode('/', filter_var( rtrim($url, '/') , FILTER_SANITIZE_URL));

$controller = ( isset($url[0]) ) ? $url[0]."_controller" : "Index_controller";
$method = ( isset($url[1]) && $url[1] != null) ? $url[1] : substr($controller, 0, -11);

$request = new Request();

$path = "./controllers/".$controller.".php";

if(file_exists($path)){

    require $path;
    $controller = new $controller();
    $controller->setPhpinput($request->phpinput);
    
    $params = $request->parameters;
    unset($params["url"]);
    
    if(isset($request->method)){
        if(method_exists($controller, $request->method)){
            if(isset($params) && $params != null){
                $controller->{$request->method}($method,$params);
            }else{
                $controller->{$request->method}($method);
            }
        }else{
            Request::error("Método no disponible",405);
        }
    } 
}else{
    Request::error("Petición no encontrada",404);
}


function ms($param) {
    $bt = debug_backtrace();
    $caller = array_shift($bt);
    echo('<pre>');
    print_r('*Call from: '.$caller['file'].'<br>*Function: '.$caller['function'].'<br>*Line: '.$caller['line'].'<br>*Message: <br>');
    print_r($param);
    echo('</pre>');
    print_r('<br>');
}