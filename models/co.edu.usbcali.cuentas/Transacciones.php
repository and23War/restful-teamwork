<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transacciones
 *
 * @author AndresG
 */
class Transacciones extends Model{
    protected static $table = "Transacciones";
    
    private $id;
    private $monto;
    private $fecha;
    /*private $coste_transaccion;
    private $Tipo_transacciones_id;
    private $Bancos_id;
    private $Cuentas_id;*/
    
    private $has_one = array(
        '$Tipo_transacciones_id'=>array(
            'class'=>'Tipo_transacciones',
            'join_as'=>'tipo_transacciones_id',
            'join_with'=>'id'
            ),
        '$Bancos_id'=>array(
            'class'=>'Bancos',
            'join_as'=>'bancos_id',
            'join_with'=>'id'
            ),
        '$Cuentas_id'=>array(
            'class'=>'Cuentas',
            'join_as'=>'cuentas_id',
            'join_with'=>'id'
            )
        );
    
    private $has_many = array(
        'cuentas'=>array(
            'class'=>'Cuentas',
            'join_self_as'=>'Transacciones_id',
            'join_other_as'=>'Cuentas_id',
            'join_table'=>'transacciones_registro'
            )
    );

    
        function __construct($id, $monto, $fecha) {
            $this->id = $id;
            $this->monto = $monto;
            $this->fecha = $fecha;
            /*$this->coste_transaccion = $coste_transaccion;
            $this->Tipo_transacciones_id = $Tipo_transacciones_id;
            $this->Bancos_id = $Bancos_id;
            $this->Cuentas_id = $Cuentas_id;*/
        }

        
    public function getMyVars(){
        return get_object_vars($this);
    }

    static function getTable() {
        return self::$table;
    }

    function getId() {
        return $this->id;
    }

    function getMonto() {
        return $this->monto;
    }

    function getFecha() {
        return $this->fecha;
    }
    function getHas_many() {
        return $this->has_many;
    }

    function setHas_many($has_many) {
        $this->has_many = $has_many;
    }

        /*function getCoste_transaccion() {
        return $this->coste_transaccion;
    }

    function getTipo_transacciones_id() {
        return $this->Tipo_transacciones_id;
    }

    function getBancos_id() {
        return $this->Bancos_id;
    }

    function getCuentas_id() {
        return $this->Cuentas_id;
    }*/

    function setId($id) {
        $this->id = $id;
    }

    function setMonto($monto) {
        $this->monto = $monto;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    /*function setCoste_transaccion($coste_transaccion) {
        $this->coste_transaccion = $coste_transaccion;
    }

    function setTipo_transacciones_id($Tipo_transacciones_id) {
        $this->Tipo_transacciones_id = $Tipo_transacciones_id;
    }

    function setBancos_id($Bancos_id) {
        $this->Bancos_id = $Bancos_id;
    }

    function setCuentas_id($Cuentas_id) {
        $this->Cuentas_id = $Cuentas_id;
    }*/



}
