<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cuentas
 *
 * @author AndresG
 */
class Cuentas extends Model{
    protected static $table = "Cuentas";
    
    private $id;
    private $password;
    private $saldo;
    private $Bancos_id;
    private $Tipo_cuentas_id;
    private $cedula;


    private $has_one = array(
        'Bancos_id'=>array(
            'class'=>'Bancos',
            'join_as'=>'Bancos_id',
            'join_with'=>'id'
            ),
        'Tipo_cuentas_id'=>array(
            'class'=>'Tipo_cuentas',
            'join_as'=>'Tipo_cuentas_id',
            'join_with'=>'id'
            )
        );
    
        function __construct($id,$cedula, $password, $saldo, $Bancos_id, $Tipo_cuentas_id) {
            $this->id = $id;
            $this->cedula = $cedula;
            $this->password = $password;
            $this->saldo = $saldo;
            $this->Bancos_id = $Bancos_id;
            $this->Tipo_cuentas_id = $Tipo_cuentas_id;
        }

                
    public function getMyVars(){
        return get_object_vars($this);
    }
        
    static function getTable() {
        return self::$table;
    }

    function getId() {
        return $this->id;
    }
    function getCedula() {
        return $this->cedula;
    }

    function getPassword() {
        return $this->password;
    }

    function getSaldo() {
        return $this->saldo;
    }

    function getBancos_id() {
        return $this->Bancos_id;
    }

    function getTipo_cuentas_id() {
        return $this->Tipo_cuentas_id;
    }

    static function setTable($table) {
        self::$table = $table;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setSaldo($saldo) {
        $this->saldo = $saldo;
    }

    function setBancos_id($Bancos_id) {
        $this->Bancos_id = $Bancos_id;
    }

    function setTipo_cuentas_id($Tipo_cuentas_id) {
        $this->Tipo_cuentas_id = $Tipo_cuentas_id;
    }
    
    function setCedula($cedula) {
        $this->cedula = $cedula;
    }



}
