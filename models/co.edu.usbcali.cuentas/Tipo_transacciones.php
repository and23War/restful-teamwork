<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tipo_transacciones
 *
 * @author AndresG
 */
class Tipo_transacciones extends Model {
    protected static $table = "tipo_transacciones";

    private $id;
    private $nombre;
    
    function __construct($id, $nombre) {
        $this->id = $id;
        $this->nombre = $nombre;
    }
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }


}
