<?php
/**
 * Description of Usuario
 *
 * @author pabhoz
 */
class Usuario extends Model{
    
    protected static $table = "Usuario";
    
    private $id;
    private $username;
    private $password;
    private $correo;
    private $nombre;
    private $sexo;
    
    private $has_one = array(
        'ciudad'=>array(
            'class'=>'Ciudad',
            'join_as'=>'id_ciudad',
            'join_with'=>'id'
            )
        );
    
        function __construct($id, $username, $password, $correo, $nombre, $sexo) {
            $this->id = $id;
            $this->username = $username;
            $this->password = $password;
            $this->correo = $correo;
            $this->nombre = $nombre;
            $this->sexo = $sexo;
        }

        
    public function getMyVars(){
        return get_object_vars($this);
    }
    
    static function getTable() {
        return self::$table;
    }

    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getSexo() {
        return $this->sexo;
    }

    static function setTable($table) {
        self::$table = $table;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }
   
}
