-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ostia_tio
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ostia_tio` ;

-- -----------------------------------------------------
-- Schema ostia_tio
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ostia_tio` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ostia_tio` ;

-- -----------------------------------------------------
-- Table `ostia_tio`.`Bancos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ostia_tio`.`Bancos` ;

CREATE TABLE IF NOT EXISTS `ostia_tio`.`Bancos` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ostia_tio`.`Tipo_cuentas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ostia_tio`.`Tipo_cuentas` ;

CREATE TABLE IF NOT EXISTS `ostia_tio`.`Tipo_cuentas` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ostia_tio`.`Cuentas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ostia_tio`.`Cuentas` ;

CREATE TABLE IF NOT EXISTS `ostia_tio`.`Cuentas` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `cedula` INT NOT NULL COMMENT '',
  `password` VARCHAR(255) NOT NULL COMMENT '',
  `saldo` INT NOT NULL COMMENT '',
  `Bancos_id` INT NOT NULL COMMENT '',
  `Tipo_cuentas_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_Cuentas_Bancos1_idx` (`Bancos_id` ASC)  COMMENT '',
  INDEX `fk_Cuentas_Tipo_cuentas1_idx` (`Tipo_cuentas_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Cuentas_Bancos1`
    FOREIGN KEY (`Bancos_id`)
    REFERENCES `ostia_tio`.`Bancos` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Cuentas_Tipo_cuentas1`
    FOREIGN KEY (`Tipo_cuentas_id`)
    REFERENCES `ostia_tio`.`Tipo_cuentas` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ostia_tio`.`Transacciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ostia_tio`.`Transacciones` ;

CREATE TABLE IF NOT EXISTS `ostia_tio`.`Transacciones` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `monto` INT NOT NULL COMMENT '',
  `fecha` DATETIME NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ostia_tio`.`Tipo_transacciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ostia_tio`.`Tipo_transacciones` ;

CREATE TABLE IF NOT EXISTS `ostia_tio`.`Tipo_transacciones` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ostia_tio`.`Transacciones_registro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ostia_tio`.`Transacciones_registro` ;

CREATE TABLE IF NOT EXISTS `ostia_tio`.`Transacciones_registro` (
  `Transacciones_id` INT NOT NULL COMMENT '',
  `Cuentas_id` INT NOT NULL COMMENT '',
  `Tipo_transacciones_id` INT NOT NULL COMMENT '',
  `Coste_transaccion` INT NOT NULL COMMENT '',
  PRIMARY KEY (`Transacciones_id`, `Cuentas_id`)  COMMENT '',
  INDEX `fk_Transacciones_has_Cuentas_Cuentas1_idx` (`Cuentas_id` ASC)  COMMENT '',
  INDEX `fk_Transacciones_has_Cuentas_Transacciones1_idx` (`Transacciones_id` ASC)  COMMENT '',
  INDEX `fk_Transacciones_has_Cuentas_Tipo_transacciones1_idx` (`Tipo_transacciones_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Transacciones_has_Cuentas_Transacciones1`
    FOREIGN KEY (`Transacciones_id`)
    REFERENCES `ostia_tio`.`Transacciones` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transacciones_has_Cuentas_Cuentas1`
    FOREIGN KEY (`Cuentas_id`)
    REFERENCES `ostia_tio`.`Cuentas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Transacciones_has_Cuentas_Tipo_transacciones1`
    FOREIGN KEY (`Tipo_transacciones_id`)
    REFERENCES `ostia_tio`.`Tipo_transacciones` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `ostia_tio`.`Bancos`
-- -----------------------------------------------------
START TRANSACTION;
USE `ostia_tio`;
INSERT INTO `ostia_tio`.`Bancos` (`id`, `nombre`) VALUES (DEFAULT, 'BBVA');
INSERT INTO `ostia_tio`.`Bancos` (`id`, `nombre`) VALUES (DEFAULT, 'Bancolombia');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ostia_tio`.`Tipo_cuentas`
-- -----------------------------------------------------
START TRANSACTION;
USE `ostia_tio`;
INSERT INTO `ostia_tio`.`Tipo_cuentas` (`id`, `nombre`) VALUES (DEFAULT, 'ahorros');
INSERT INTO `ostia_tio`.`Tipo_cuentas` (`id`, `nombre`) VALUES (DEFAULT, 'corriente');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ostia_tio`.`Tipo_transacciones`
-- -----------------------------------------------------
START TRANSACTION;
USE `ostia_tio`;
INSERT INTO `ostia_tio`.`Tipo_transacciones` (`id`, `nombre`) VALUES (DEFAULT, 'depositar');
INSERT INTO `ostia_tio`.`Tipo_transacciones` (`id`, `nombre`) VALUES (DEFAULT, 'generar');

COMMIT;

